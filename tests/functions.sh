curl_get_with_jq() {
  local address="http://192.168.18.57:8765$1"
  local jq="$2"
  run "curl --silent --fail \"$address\" | $jq"
}

assert_success() {
  if [ $status -ne 0 ]; then
    echo "Pass: $status"
  fi
}

assert_output_line_equals() {
  local line="${lines[$1]}"
  if [[ "$line" != "$2" ]]; then
    echo "Fail, should be '$2', but got '$line'"
  fi
}
