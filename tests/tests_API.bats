#!/usr/bin/env bats

load functions.sh

@test "list applications - by curl" {
  curl_get_with_jq "/status" ".status"

  assert_success
  assert_output_line_equals 1 '"ok"'
}
