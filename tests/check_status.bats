#!/usr/bin/env bats

@test "Check if http://192.168.18.57:8765/ready returns JSON {'status':'ok'}" {
  result="$(curl -s http://192.168.18.57:8765/ready)"
  expected='{"status":"ok"}'

  if [ "$result" == "$expected" ]; then
    echo "OK"
  else
    echo "Failed: Expected '$expected', but got '$result'"
    exit 1
  fi
}